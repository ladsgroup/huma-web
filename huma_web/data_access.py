import os
from pathlib import Path
import json

import psycopg

dir = Path(os.path.dirname(os.path.abspath(__file__))).parent

with open(os.path.join(dir, 'config.json')) as f:
    db_config = json.loads(f.read())['db']

def run_sql(sql, values=None):
    if not sql.strip().endswith(';'):
        sql += ';'
    dbname = db_config['db']
    user = db_config['user']
    password = db_config['password']
    host = db_config['host']
    with psycopg.connect(f"dbname={dbname} user={user} password={password} host={host}") as conn:
        with conn.cursor() as cur:
            if not values:
                cur.execute(sql)
            else:
                cur.execute(sql, values)
            return (cur.fetchall(), [desc[0] for desc in cur.description])
