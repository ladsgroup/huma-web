from huma_web.data_access import run_sql


def get_callers(class_name, method_name=None):
    sql = """select concat(caller_class_name, '::', caller_method_name) as caller,
concat(called_class_name, '::', called_method_name) as called
from calls
where called_class_name = %s"""
    if method_name:
        sql += ' and called_method_name = %s'
        res = run_sql(sql, [class_name, method_name])[0]
    else:
        res = run_sql(sql, [class_name])[0]
    return res