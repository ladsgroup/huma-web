import os
import json
import urllib.parse

from flask import Flask, render_template, request
import psycopg.errors

from huma_web.callers import get_callers
from huma_web.data_access import run_sql


dir = os.path.dirname(os.path.abspath(__file__))
with open(os.path.join(dir, 'examples.json')) as f:
    examples = json.loads(f.read())

app = Flask(__name__)
dir = os.path.dirname(os.path.abspath(__file__))



@app.route("/")
def hello():
    return render_template('home.html')


@app.route("/callers", methods=['GET'])
def callers():
    class_name = request.args.get('class_name')
    method_name = request.args.get('method_name')
    if not class_name:
        return render_template('callers.html')
    return render_template('callers_results.html', results=get_callers(class_name, method_name), method_name=urllib.parse.quote_plus(method_name), class_name=urllib.parse.quote_plus(class_name))


@app.route("/sql", methods=['GET'])
def run_sql_endpoint():
    sql = request.args.get('sql')
    if not sql:
        return render_template('sql.html')
    try:
        results = run_sql(sql)
    except Exception as error:
        print(str(type(error)))
        return render_template('error_sql.html', error=error, sql=sql)
    else:
        return render_template('run_sql.html', results=results, sql_url=urllib.parse.quote_plus(sql), sql=sql)


@app.route("/examples", methods=['GET'])
def examples_endpoint():
    return render_template('examples.html', examples=examples)
    

@app.route('/download/callers/json')
def download_callers_json():
    class_name = request.args.get('class_name')
    method_name = request.args.get('method_name')
    if not class_name:
        return render_template('callers.html')
    return json.dumps(get_callers(class_name, method_name)), {"Content-Type": "application/json"}


@app.route("/download/sql/json", methods=['GET'])
def download_run_sql():
    sql = request.args.get('sql')
    if not sql:
        return render_template('sql.html')
    return json.dumps(run_sql(sql)[0]), {"Content-Type": "application/json"}

if __name__ == "__main__":
    app.run(host='0.0.0.0')
